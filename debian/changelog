nvidia-cg-toolkit (3.1.0013-5) unstable; urgency=medium

  * Switch to debhelper-compat (= 12).
  * Bump Standards-Version to 4.3.0. No changes needed.
  * Add Build-Depends-Package field to symbols files.
  * Add debian/rules targets for archiving the tarballs in a separate
    repository using git-lfs as storage backend.
  * Update Lintian overrides.

 -- Andreas Beckmann <anbe@debian.org>  Mon, 28 Jan 2019 02:32:48 +0100

nvidia-cg-toolkit (3.1.0013-4) unstable; urgency=medium

  * Switch to debhelper compat level 11.
  * Bump Standards-Version to 4.1.3. No changes needed.
  * Switch Vcs-* URLs to salsa.debian.org.
  * Update watch file.
  * Fix new Lintian issues.

 -- Andreas Beckmann <anbe@debian.org>  Fri, 16 Mar 2018 07:30:55 +0100

nvidia-cg-toolkit (3.1.0013-3) unstable; urgency=medium

  [ Andreas Beckmann ]
  * Add gbp.conf to enable overlay mode.
  * Add get-tarball-dir target to checkout the tarball archive from SVN.
  * Set Priority to optional.
  * Bump Standards-Version to 4.1.1.
  * Use https:// URLs where possible.
  * Bump debhelper to (>= 10) and compat version to 10.
  * Set Rules-Requires-Root: no.
  * Enable more hardening.
  * Use dpkg makefile snippets instead of manual changelog parsing.
  * Remove maintainer scripts needed for upgrades from ancient version.
  * Fix typo in manpage.
  * Update Lintian overrides.

  [ Russ Allbery ]
  * Remove myself from Uploaders.

 -- Andreas Beckmann <anbe@debian.org>  Fri, 17 Nov 2017 06:38:24 +0100

nvidia-cg-toolkit (3.1.0013-2) unstable; urgency=low

  [ Miguel A. Colón Vélez ]
  * debian/control:
    - Build depend on libglu-dev instead of libglu1-dev.

  [ Andreas Beckmann ]
  * Use canonical Vcs-* URLs.
  * Bump Standards-Version to 3.9.6. No changes needed.
  * Fix manpage typos.
  * Update Lintian overrides.
  * Update my email address and drop DMUA.

 -- Andreas Beckmann <anbe@debian.org>  Sat, 18 Oct 2014 15:47:46 +0200

nvidia-cg-toolkit (3.1.0013-1) unstable; urgency=low

  [ Andreas Beckmann ]
  * Adopt package.  (Closes: #649514)
  * New Maintainer: Debian NVIDIA Maintainers.
  * New Uploaders: Miguel A. Colón Vélez, Russ Allbery, and myself.
  * Move packaging git repository to pkg-nvidia:
    git://git.debian.org/git/pkg-nvidia/nvidia-cg-toolkit.git
  * Acknowledge l10n NMUs.  (Closes: #610093)
  * Update Lintian overrides.
  * Review, reorder, and simplify the packaging.
  * Add XS-Autobuild: yes.
  * Use Breaks/Replaces: nvidia-cg-toolkit (<< ${source:Version}) to allow
    upgrades from old monolithic packages in Ubuntu and from NVIDIA.
  * Document a possible package build workflow in README.source.
  * Error out early if the .orig-*.tar.gz are not unpacked before build.

  [ Miguel A. Colón Vélez ]
  * New upstream release 3.1.0013 (April 2012).
    - Since release 2.1 the upstream license allows redistribution, so convert
      the packaging from an installer package (in contrib) to a set of
      packages: nvidia-cg-{toolkit,dev,doc}, libcg, libcggl (in non-free).
    (Closes: #506494, #539418, #502457, #639857)
  * Remove installer script, debconf translations and related dependencies.
  * nvidia-cg-toolkit.preinst: Run installer script in uninstallation mode
    before upgrade.
  * Convert package to 3.0 (quilt) format.
  * Add watch file, get-orig-source target, and README.source.
  * Use two pristine upstream tarballs (*.orig-{amd64,i386}.tar.gz) and an
    empty *.orig.tar.gz (via create-empty-orig in debian/source/options).
  * Bump Standards-Version to 3.9.3.
  * Update copyright file as per DEP-5.
  * Bump debhelper to (>= 9) and compat version to 9.
  * Simplify the rules file by using the dh helper.
  * Change Section from contrib/libs to non-free/libs.
  * Update the README.Debian file and Homepage.
  * Add build dependencies for cginfo and cgfxcat.
  * Compile cginfo and cgfxcat instead of using the prebuilt binaries.
    - Ensure the hardened LDFLAGS are used.
    - Link cginfo and cgfxcat with --as-needed to reduce library footprint.
  * Don't strip the prebuilt binaries since stripping may violate the license
    (modifies binaries).
  * Use the manpages from version 3.0.0016 for the binaries in the
    nvidia-cg-toolkit package. They got removed upstream in version 3.1.0010 but
    are still included in the Cg Reference Manual.
    - Fix a spelling error in cgc.1 (compatable -> compatible).
  * Add multiarch support.
  * Create the libcg and libcggl packages.
    - Unfortunately upstream provides libraries with an unversioned SONAME.
    - Use Multi-Arch: same.
    - Breaks/Replaces old nvidia-cg-toolkit and libcg.
    - Create the libcg.symbols and libcggl.symbols files.
    - Create *.postinst/*.postrm according to Debian policy 8.1.1.
  * Create the nvidia-cg-dev package.
    - Use Multi-Arch: same.
    - Depend on the new libcg and libcggl packages.
    - Breaks/Replaces old nvidia-cg-toolkit.
  * Create the nvidia-cg-doc package.
    - Breaks/Replaces old nvidia-cg-toolkit.
    - Don't compress the examples (Makefile, *.h, *.c, *.cpp, *.cg).
    - Don't include the license.pdf/license.txt files since a verbatim copy is
      included in the copyright file.
    - Don't include the Microsoft Visual Studio files.
    - Don't include architecture dependent files. These files can be
      recompiled by the user since the sources are included.
  * Package nvidia-cg-toolkit:
    - Change Section to non-free/devel.
    - Depend on the new libcg, libcggl and nvidia-cg-dev packages.
    - Suggest the newly created nvidia-cg-doc package.
    - Update description using the latest information from the Nvidia website.
  * {libcg,nvidia-cg-doc}.postinst: Manually remove /usr/lib/libCg.so and
    /usr/lib/libCgGL.so. Due to the removal of the /usr/lib64 symlink, the
    installer script does not remove these files.

  [ Russ Allbery ]
  * Add DM-Upload-Allowed: yes.

 -- Miguel A. Colón Vélez <debian.micove@gmail.com>  Thu, 24 May 2012 20:45:17 -0400

nvidia-cg-toolkit (2.1.0017.deb1+nmu3) unstable; urgency=low

  * Non-maintainer upload.
  * Fix encoding of Danish debconf translation.

 -- Christian Perrier <bubulle@debian.org>  Tue, 11 Jan 2011 22:05:15 +0100

nvidia-cg-toolkit (2.1.0017.deb1+nmu2) unstable; urgency=low

  * Non-maintainer upload.
  * Fix pending l10n issues. Debconf translations:
    - Italian (Vincenzo Campanella).  Closes: #560361
    - Vietnamese (Clytie Siddall).  Closes: #569653
    - Danish (Joe Hansen).  Closes: #582866

 -- Christian Perrier <bubulle@debian.org>  Mon, 31 May 2010 07:18:27 +0200

nvidia-cg-toolkit (2.1.0017.deb1+nmu1) unstable; urgency=low

  * Non-maintainer upload.
  * Fix pending l10n issues. Debconf translations:
    - Galician (Marce Villarino).  Closes: #512440
    - Czech (Miroslav Kure).  Closes: #559608
    - Spanish (Francisco Javier Cuadrado).  Closes: #559866

 -- Christian Perrier <bubulle@debian.org>  Tue, 08 Dec 2009 22:10:58 +0100

nvidia-cg-toolkit (2.1.0017.deb1) unstable; urgency=low

  * Fixed installer for Cg release 2.1.0017
  * Fixed all lintian warnings

 -- Federico Di Gregorio <fog@debian.org>  Thu, 19 Mar 2009 00:06:30 +0100

nvidia-cg-toolkit (2.0.0015.deb3) unstable; urgency=low

  * Fixed all gentle warnings in script found with Perl::Critic.
  * This also fixes some bugs reporting Perl warnings.
    (Closes: #502234)
    (Closes: #503331)

 -- Andres Mejia <mcitadel@gmail.com>  Sun, 16 Nov 2008 10:07:18 -0500

nvidia-cg-toolkit (2.0.0015.deb2) unstable; urgency=low

  * Adding Japanese translations. (Closes: #493566)
  * Addressing command-with-path-in-maintainer-script lintian warning.

 -- Andres Mejia <mcitadel@gmail.com>  Thu, 09 Oct 2008 14:35:31 -0400

nvidia-cg-toolkit (2.0.0015.deb1) unstable; urgency=low

  * New upstream release. Closes: #482661
  * Provide license documentation provided in Cg toolkit in copyright file. Also
    provide copyright information.
  * Use ${misc:Depends} to add debconf dependencies.
  * Add updated Russian translations. Closes: #487122
  * Add updated Swedish translations. Closes: #488180
  * Add updated Portuguese translations. Closes: #489428
  * Bumped Standards-Version to 3.8.0.
  * Edited description to remove specific release notes.
  * Updated both manifest files.
  * Edit README.Debian and add note about availability of book
    "The Cg Tutorial".
  * Register documentation with doc-base.

 -- Andres Mejia <mcitadel@gmail.com>  Wed, 23 Jul 2008 11:55:46 -0400

nvidia-cg-toolkit (2.0.0012.deb6) unstable; urgency=low

  * Include updated french debconf translation. Closes: #480122

 -- Andres Mejia <mcitadel@gmail.com>  Thu, 08 May 2008 15:48:09 -0400

nvidia-cg-toolkit (2.0.0012.deb5) unstable; urgency=low

  * Include updated german debconf translation. Closes: #479234
  * Include DM-Upload-Allowed: yes field.

 -- Andres Mejia <mcitadel@gmail.com>  Tue, 06 May 2008 14:04:15 -0400

nvidia-cg-toolkit (2.0.0012.deb4) unstable; urgency=low

  * Include ftp as part of proxy scheme when using proxy option.
  * Don't append downloads to file, just overwrite.
  * Fix problems with downloads. Closes: #471339
    + Needed to use Time::HiRes to prevent problem where script divided by zero.
  * Leave a copy of toolkit and spec file in /var/cache/nvidia-cg-toolkit.

 -- Andres Mejia <mcitadel@gmail.com>  Fri, 25 Apr 2008 15:46:01 -0400

nvidia-cg-toolkit (2.0.0012.deb3) unstable; urgency=low

  [ Andres Mejia ]
  * Fixed problem where --proxy option was being used when unnecessary.
  * Updated nvidia-cg-toolkit/http_proxy template.
  * Included updated Russian, German, and French translations.
    Closes: #471026
    Closes: #471180
    Closes: #472127
  * Minor update to TODO.Debian.
  * Script can now generate a manifest file.
    + Also writes combined total size of files that are installed.
  * Use recursive strategy for copying directories. No longer need
    libfile-copy-recursive-perl.
  * Compress certain documentation that are over 4kb in size.
  * Use maximum compression when compressing files.
  * Show progress when downloading files.
  * Print certain messages to STDERR.
  * Determine "release" information for generating manual page using
    dpkg-parsechangelog.
  * Added updated legal notice from NVIDIA website to copyright file.

 -- Andres Mejia <mcitadel@gmail.com>  Mon, 07 Apr 2008 20:05:37 -0400

nvidia-cg-toolkit (2.0.0012.deb2) unstable; urgency=low

  [ Andres Mejia ]
  * Fixed binary name in manual page and when specifying --help option for
    nvidia-cg-toolkit-installer.
  * Added another TODO.

 -- Andres Mejia <mcitadel@gmail.com>  Sat, 01 Mar 2008 16:26:28 -0500

nvidia-cg-toolkit (2.0.0012.deb1) unstable; urgency=low

  [ Andres Mejia ]
  * Changed Priority to extra.
  * Added proxy support. Closes: #462271
    + Using Perl module LWP::UserAgent for proxy support, thus
      update-nvidia-cg-toolkit has been converted to Perl script.
  * Renamed from update-nvidia-cg-toolkit to nvidia-cg-toolkit-installer.
    + Maintainer scripts have been updated for new script.
  * Old scripts have been deleted.
  * Updated control file for required Perl module packages.
  * Updated copyright file for new installer script.
  * Setting correct way to use proxy in postinst file.
  * Took out mention of older versions of NVIDIA Cg Toolkit from template file.
  * Grammer check done for template file.
  * Generate a man page using pod2man from nvidia-cg-toolkit-installer.
  * Specify a deb revision as *.debN so as not to cause confusion as to what
    version of the NVIDIA Cg Toolkit this installs.
  * Added TODO.Debian.
  * Closing bug about not being able to install after sid update.
    + A new script is about to be used. Please reopen if issue still occurs.
    + Closes: #465971
  * Adding README file from NVIDIA Cg Toolkit to documentation.
  * Making the Cg compiler executable.

 -- Andres Mejia <mcitadel@gmail.com>  Sat, 23 Feb 2008 19:51:02 -0500

nvidia-cg-toolkit (2.0.0010) unstable; urgency=low

  [ Andres Mejia ]
  * New upstream version. Closes: #458907
  * It's possible to specify an http proxy when installing this package.
    Closes: #336072
  * Bumped Standards-Version to 3.7.3.
  * Added Homepage field in control file.
  * Added Vcs-{Git,Browser} field in control file.
  * Updated Description field for new Cg toolkit version.
    + Also modified description to state that it's only an installer.
      Closes: #309924
  * Fixed some grammatical errors in the copyright file.
    + Also recopied license from NVIDIA website and included information
      on the date the license was last modified.
  * Edited README.Debian to include contact information on where to report
    problems with the toolkit.
  * Removed redundant link to the homepage from README.Debian.
  * Changed path for update-nvidia-cg-toolkit, should be in /usr/bin.
  * Adding lintian override for directories left intentionally blank.
  * Fixed debian/rules, removing some unnecessary commands.
  * Add Provides and Conflicts field for libcg and libcggl, which is installed
    by the installer.
  * Use debhelper (>> 4.1.16), silences dpkg warning.
  * Script will also download spec file.
    + Changed some option parameters
  * Script will remove temporary directories if they were used.
  * Package installation will delete downloaded files by default to comply
    with the upstream license.
  * Script will run ldconfig at the end of install or uninstall.

 -- Andres Mejia <mcitadel@gmail.com>  Tue, 08 Jan 2008 16:19:02 -0500

nvidia-cg-toolkit (1.5.0023) unstable; urgency=low

  [ Andres Mejia ]
  * New upstream version. Closes: #446121
  * Bumped Standards-Version to 3.7.2.
  * Added myself in uploaders field.
  * Removing debian revision part in changelog version.
    + Silences lintian warning: native-package-with-dash-version.
  * Removing build dependency on docbook-to-man (wasn't being used).
  * Providing man page for update-nvidia-cg-toolkit.

 -- Andres Mejia <mcitadel@gmail.com>  Thu, 22 Nov 2007 22:13:32 -0500

nvidia-cg-toolkit (1.5.0.0019-1) unstable; urgency=low

  * New upstream version. Closes: #360077, #389106
  * Updated package description including Cg description from NVIDIA
    README file. Closes: #302112
  * Included updated spanish translation. Closes: #413780

 -- Federico Di Gregorio <fog@debian.org>  Wed, 07 Mar 2007 10:23:05 +0100

nvidia-cg-toolkit (1.3.0501.0700-3.2) unstable; urgency=low

  * Non-maintainer upload to include the French translation forgotten
    by the former NMU
  * French debconf transalation added.

 -- Christian Perrier <bubulle@debian.org>  Fri, 24 Nov 2006 14:18:17 +0100

nvidia-cg-toolkit (1.3.0501.0700-3.1) unstable; urgency=low

  * Non-maintainer upload to fix longstanding l10n issues
  * Turn one debconf template to the error type. Closes: #388947
  * Rewrite the debconf templates to fix the Developer's Reference
    suggestions.
  * Switch to po-debconf for debconf templates translations
    Closes: #351388
  * Debconf translation updates:
    - Swedish. Closes: #331008
    - Czech added. Sent during the call for updates of the NMU campaign.
    - Brazilian Portuguese added. Sent during the call for updates of
      the NMU campaign.
    - Portuguese added. Sent during the call for updates of the NMU campaign.
    - Vietnamese added. Sent during the call for updates of the NMU campaign.
    - Russian added. Sent during the call for updates of the NMU campaign.
    - Swedish added. Sent during the call for updates of the NMU campaign.
    - german added. Sent during the call for updates of the NMU campaign.
  * Lintian fixes:
    - Use 4 as debhelper compatibility though debian/compat

 -- Christian Perrier <bubulle@debian.org>  Sat, 18 Nov 2006 12:14:46 +0100

nvidia-cg-toolkit (1.3.0501.0700-3) unstable; urgency=low

  * Added dependency on debconf-2.0. (Closes: #332050)

 -- Federico Di Gregorio <fog@debian.org>  Wed, 05 Oct 2005 10:22:00 +0200

nvidia-cg-toolkit (1.3.0501.0700-2) unstable; urgency=low

  * Applied patch from Drew Hess to fix a path bug.

 -- Federico Di Gregorio <fog@debian.org>  Thu, 19 May 2005 10:08:09 +0200

nvidia-cg-toolkit (1.3.0501.0700-1) unstable; urgency=low

  * New upstream release of the Cg tools from NVIDIA.

 -- Federico Di Gregorio <fog@debian.org>  Sun, 27 Feb 2005 16:46:29 +0100

nvidia-cg-toolkit (1.3.0408.0400-3) unstable; urgency=low

  * Added copy of legal information from NVIDIA website.
  * Now libraries are copied to /usr/lib64 if the target architecture
    is amd64.
  * Removed ia64 from supported architectures, NVIDIA does not provide
    libraries for it.

 -- Federico Di Gregorio <fog@debian.org>  Sat, 18 Dec 2004 15:28:43 +0100

nvidia-cg-toolkit (1.3.0408.0400-2) experimental; urgency=low

  * Added support for 64 bit architechtures (amd64).

 -- Federico Di Gregorio <fog@initd.org>  Mon, 20 Sep 2004 19:32:38 +0200

nvidia-cg-toolkit (1.3.0408.0400-1) experimental; urgency=low

  * Initial release.

 -- Federico Di Gregorio <fog@initd.org>  Mon, 20 Sep 2004 10:16:58 +0200
